package com.example.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Salary")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "employee_code")
    private Integer employeeCode;

    @Column(name = "employee_name")
    private String employeeName;

    @Column(name = "salary")
    private Double salary;

    @Column(name = "month")
    private Integer month;

    @Column(name = "year")
    private Integer year;

    @Column(name = "company")
    private String company;

//    @OneToMany(mappedBy = "salary" , fetch = FetchType.EAGER , cascade = CascadeType.ALL)
//    @JsonIgnore
//    private List<Employee> employees ;



}
