package com.example.entity;

import com.example.DTO.Req.GroupEmployeeCreateDTO;
import com.example.DTO.Req.GroupReqDTO;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Groups")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Group extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "group_code")
    private String groupCode;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "division_code")
    private Integer divisionCode;

    @Column(name = "is_active")
    private Boolean isActive;

    @OneToMany(mappedBy = "group",fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private List<GroupEmployee> groupEmployeeList;

    public Group(GroupReqDTO groupReqDTO){
        this.divisionCode = groupReqDTO.getDivisionCode();
        this.isActive = groupReqDTO.getIsActive();
    }

}
