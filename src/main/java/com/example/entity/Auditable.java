package com.example.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * Class định nghĩa về thời gian và người tiến hành create, update
 *
 * @author dungnt
 * @since 30/11/2021
 */
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {
  @CreatedBy
  @Column(name = "created_by", nullable = false, updatable = false)
  private U createdBy;

  @CreatedDate
  @Column(name = "created_date", nullable = false, updatable = false)
  private LocalDateTime createdDate;

  @LastModifiedBy
  @Column(name = "updated_by")
  private U updatedBy;

  @LastModifiedDate
  @Column(name = "updated_date")
  private LocalDateTime updatedDate;
}
