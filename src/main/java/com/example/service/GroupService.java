package com.example.service;

import com.example.DTO.GroupDTO;
import com.example.DTO.GroupEmployeeDTO;
import com.example.DTO.Req.GroupEmployeeReqDTO;
import com.example.DTO.Req.GroupReqDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GroupService {

    void create(GroupReqDTO groupReqDTO) throws Exception;

    void update(Integer id, GroupEmployeeReqDTO groupEmployeeReqDTOList ) throws Exception;

    void delete(Integer id) throws Exception;


}
