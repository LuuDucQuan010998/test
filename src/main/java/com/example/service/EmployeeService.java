package com.example.service;

import com.example.DTO.EmployeeDTO;
import com.example.DTO.Req.EmployeeReqDTO;
import com.example.exceptions.EmployeeException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface EmployeeService {
    void create(EmployeeReqDTO employeeReqDTO) throws EmployeeException;

    EmployeeDTO update(EmployeeReqDTO employeeReqDTO , Integer id);

    void delete(Integer id);

    Page<EmployeeDTO> search(String name, Integer code, Pageable pageable);

}
