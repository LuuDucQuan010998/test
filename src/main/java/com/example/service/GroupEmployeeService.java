package com.example.service;

import com.example.DTO.GroupEmployeeDTO;
import com.example.DTO.Req.GroupEmployeeReqDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GroupEmployeeService {
    GroupEmployeeDTO create(GroupEmployeeDTO groupEmployeeReqDTO);

    GroupEmployeeDTO update(Integer id, GroupEmployeeDTO groupEmployeeReqDTO);

    void delete(Integer id);
}
