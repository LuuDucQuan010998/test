package com.example.service;

import com.example.DTO.CategoryDTO;
import com.example.DTO.Req.CategoryRedDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface CategoryService {
    CategoryDTO create(CategoryRedDTO categoryRedDTO);

    CategoryDTO update(CategoryRedDTO categoryRedDTO , Integer id);

    void delete(Integer id);

    Page<CategoryDTO> search(String name, Pageable pageable);
}
