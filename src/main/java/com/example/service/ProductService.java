package com.example.service;

import com.example.DTO.ProductDTO;
import com.example.DTO.Req.ProductRedDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface ProductService {
    ProductDTO create(ProductRedDTO productRedDTO);

    ProductDTO update(ProductRedDTO productRedDTO , Integer id);

    void delete(Integer id);

    Page<ProductDTO> search(String name, Pageable pageable);
}
