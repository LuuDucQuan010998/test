package com.example.service;

import com.example.DTO.Req.SalaryReqDTO;
import com.example.DTO.SalaryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SalaryService {

    List<SalaryDTO> getAll() ;

    SalaryDTO add(SalaryReqDTO salaryCreateReqDTO) ;

    SalaryDTO update(int id, SalaryReqDTO salaryCreateReqDTO);

    void delete(int id) ;

    Page<SalaryDTO> search(Integer code , String name, Pageable pageable);

    Page<SalaryDTO> search2(Integer code , String name, Pageable pageable);

    List<SalaryDTO> getName(String name, int code);

    double sumSalary();

    Page<SalaryDTO> getAllandPase();
}
