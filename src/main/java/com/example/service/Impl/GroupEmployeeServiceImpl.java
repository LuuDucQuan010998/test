package com.example.service.Impl;

import com.example.DTO.GroupEmployeeDTO;
import com.example.DTO.Req.GroupEmployeeReqDTO;
import com.example.entity.GroupEmployee;
import com.example.repository.EmployeeRepository;
import com.example.repository.GroupEmployeeRepository;
import com.example.service.GroupEmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Component
public class GroupEmployeeServiceImpl implements GroupEmployeeService {

    private final ModelMapper modelMapper;

    private final GroupEmployeeRepository groupEmployeeRepository;

    private final EmployeeRepository employeeRepository;

    @Override
    public GroupEmployeeDTO create(GroupEmployeeDTO groupEmployeeReqDTO) {
            return Optional.of(groupEmployeeReqDTO)
                    .map(groupEmployeeReq -> modelMapper.map(groupEmployeeReq, GroupEmployee.class))
                    .map(groupEmployeeRepository::save)
                    .map(group -> modelMapper.map(group,GroupEmployeeDTO.class))
                    .orElseThrow();

    }



    @Override
    public GroupEmployeeDTO update(Integer id, GroupEmployeeDTO groupEmployeeReqDTO) {
        id = groupEmployeeReqDTO.getId();
        return Optional.of(groupEmployeeReqDTO)
                .map(groupEmployeeReq -> modelMapper.map(groupEmployeeReq, GroupEmployee.class))
                .map(groupEmployeeRepository::save)
                .map(groupEmployee -> modelMapper.map(groupEmployee,GroupEmployeeDTO.class))
                .orElseThrow();
    }

    @Override
    public void delete(Integer id) {
        groupEmployeeRepository.deleteById(id);
    }
}
