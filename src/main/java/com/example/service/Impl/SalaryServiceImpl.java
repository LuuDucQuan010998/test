package com.example.service.Impl;

import com.example.DTO.Req.SalaryReqDTO;
import com.example.DTO.SalaryDTO;
import com.example.entity.Employee;
import com.example.entity.Salary;
import com.example.repository.SalaryRepository;
import com.example.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Component
public class SalaryServiceImpl implements SalaryService {
    private final SalaryRepository salaryRepository;

    private final JdbcTemplate jdbcTemplate;

    private final ModelMapper modelMapper;


    @Override
    public List<SalaryDTO> getAll() {
        return salaryRepository.findAll().stream().map(salary -> modelMapper.map(salary, SalaryDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public SalaryDTO add(SalaryReqDTO salaryCreateReqDTO) {
        //Validate req
        //Map req to model
        //save model
        //map model to resDTO

//        Salary salary = modelMapper.map(salaryCreateReqDTO, Salary.class);
//        for (Employee employee : salary.getEmployees()) {
//            employee.setSalary(salary);
//        }
//        salaryRepository.save(salary);
//        SalaryDTO salaryDTO = modelMapper.map(salary,SalaryDTO.class);
//        return salaryDTO;
//        return Optional.of(salaryCreateReqDTO)
//                .map(salaryReq -> modelMapper.map(salaryReq, Salary.class))
//                .map(salary -> {
//                    for (Employee employee : salary.getEmployees()) {
//                    employee.setSalary(salary);
//                 }
//                    return  salaryRepository.save(salary);
//                })
//                .map(salaryRepository::save)
//                .map(salary -> modelMapper.map(salary, SalaryDTO.class))
//                .orElseThrow();
        return  null;

    }

    @Override
    public SalaryDTO update(int id, SalaryReqDTO salaryCreateReqDTO) {
        id = salaryCreateReqDTO.getId();
        return Optional.of(salaryCreateReqDTO)
                .map(salaryReq -> modelMapper.map(salaryReq, Salary.class))
                .map(salaryRepository::save)
                .map(salary -> modelMapper.map(salary, SalaryDTO.class))
                .orElseThrow();
    }

    @Override
    public void delete(int id) {
        salaryRepository.deleteById(id);
    }

    @Override
    public Page<SalaryDTO> search(Integer code , String name, Pageable pageable) {
        // dùng cách a vừa chỉ sẽ phức tạp hơn đoạn chia page
                // chia page chưa xong e , còn cái tính count nhé .
        String sql = "SELECT * FROM Salary where 1=1";
//        StringUtils.isNotBlank(null)      = false
//        StringUtils.isNotBlank("")        = false
//        StringUtils.isNotBlank(" ")       = false
//        StringUtils.isNotBlank("bob")     = true
//        StringUtils.isNotBlank("  bob  ") = true
        // ->> nó bao quá được hết các tường hợp
        if (StringUtils.isNotBlank(name)){
            sql = sql + " and employee_name = '" + name + "'";
        }
        if(code != null){
            sql = sql + " and employee_code = '" + code + "'";
        }
        Integer count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM Salary where 1=1",Integer.class);
        System.out.println(sql);
        // query ra entities
        List<Salary> salaries = jdbcTemplate.query(
                sql,
                (rs, rowNum) ->
                        new Salary(
                                rs.getInt("id"),
                                rs.getInt("employee_code"),
                                rs.getString("employee_name"),
                                rs.getDouble("salary"),
                                rs.getInt("month"),
                                rs.getInt("year"),
                                rs.getString("company")

                        )
        );
        // mapping vs dto để trả về respóne
        List<SalaryDTO> content = salaries.stream().map(i -> modelMapper.map(i, SalaryDTO.class)).collect(Collectors.toList());
        return new PageImpl<>(content,pageable,count);
    }

    @Override
    public List<SalaryDTO> getName(String name, int code ) {
        return salaryRepository.findByName(name, code ).stream().map(salary -> modelMapper.map(salary, SalaryDTO.class)).collect(Collectors.toList());
    }

    @Override
    public double sumSalary() {
        List<SalaryDTO> listSaraly = salaryRepository.findAll().stream().map(salary -> modelMapper.map(salary, SalaryDTO.class))
                .collect(Collectors.toList());
        return listSaraly.stream().mapToDouble(p -> p.getSalary()).sum();
    }

    @Override
    public Page<SalaryDTO> getAllandPase() {
        Pageable pageable = PageRequest.of(0, 5);
        Page<Salary> salarys = salaryRepository.findSalarys(pageable);
        // map lại sang DTO
       return salarys.map(salary -> modelMapper.map(salary, SalaryDTO.class));

    }


    @Override
    public Page<SalaryDTO> search2(Integer code , String name, Pageable pageable) {
        Specification<Salary> specification = (root, query, criteriaBuilder) -> {
            // khởi tao danh sách chứa điều kiện tìm kiếm
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.isNotBlank(name)){
                predicates.add(criteriaBuilder.equal(root.get("employeeName"),name));
            }
            if (code != null ){
                predicates.add(criteriaBuilder.equal(root.get("employeeCode"),code));
            }

            // bla bla add query xong buid ra câu truy vấn
            return query.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
        // tìm kiếm + phân trang luôn
        Page<Salary> salaries = salaryRepository.findAll(specification, pageable);
        // mapping dữ liệu lại về dto để trả ra
        return  salaries.map(i -> modelMapper.map(i,SalaryDTO.class));
    }

    public Page<SalaryDTO> search3(Integer code , String name, Pageable pageable){
        Specification<Salary> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(StringUtils.isNotBlank(name)){
                predicates.add(criteriaBuilder.equal(root.get("employeeName"),name));
            }
          return query.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
        return salaryRepository.findAll(specification,pageable).map(i -> modelMapper.map(i,SalaryDTO.class));
    }

}
