package com.example.service.Impl;

import com.example.DTO.EmployeeDTO;
import com.example.DTO.ProductDTO;
import com.example.DTO.Req.ProductRedDTO;
import com.example.entity.Category;
import com.example.entity.Employee;
import com.example.entity.Product;
import com.example.repository.CategoryRepository;
import com.example.repository.ProductRepository;
import com.example.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Component
public class ProductServiceImpl implements ProductService {

    private final ModelMapper modelMapper;

    private final ProductRepository productRepository;

    private final CategoryRepository categoryRepository;

    @Override
    public ProductDTO create(ProductRedDTO productRedDTO) {

        return Optional.of(productRedDTO)
                .map(productRed -> modelMapper.map(productRed, Product.class))
                .map(product -> {
                    for (Category c : product.getCategoryList()){
                        c.setProductList((List<Product>) product);
                    }
                    return productRepository.save(product);
                }).map(productRepository::save)
                .map(product -> modelMapper.map(product,ProductDTO.class))
                .orElseThrow();
    }

    @Override
    public ProductDTO update(ProductRedDTO productRedDTO, Integer id) {
        id = productRedDTO.getId();
        return Optional.of(productRedDTO)
                .map(productRed -> modelMapper.map(productRed, Product.class))
                .map(productRepository::save)
                .map(product -> modelMapper.map(product, ProductDTO.class))
                .orElseThrow();
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<ProductDTO> search(String name, Pageable pageable) {
        return null;
    }
}
