package com.example.service.Impl;

import com.example.DTO.CategoryDTO;
import com.example.DTO.Req.CategoryRedDTO;
import com.example.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Component
public class CategoryServiceImpl implements CategoryService {
    @Override
    public CategoryDTO create(CategoryRedDTO categoryRedDTO) {
        return null;
    }

    @Override
    public CategoryDTO update(CategoryRedDTO categoryRedDTO, Integer id) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Page<CategoryDTO> search(String name, Pageable pageable) {
        return null;
    }
}
