package com.example.service.Impl;

import com.example.DTO.EmployeeDTO;
import com.example.DTO.Req.EmployeeReqDTO;
import com.example.entity.Employee;
import com.example.exceptions.EmployeeException;
import com.example.repository.EmployeeRepository;
import com.example.repository.SalaryRepository;
import com.example.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Component
public class EmployeeServiceImpl implements EmployeeService {

    final private EmployeeRepository employeeRepository;

    final private ModelMapper modelMapper;

    private final SalaryRepository salaryRepository;

    @Override
    public void create(EmployeeReqDTO employeeReqDTO) throws EmployeeException {
        if(employeeRepository.existsByHrisCode(employeeReqDTO.getHrisCode())){
           throw new EmployeeException("Nhân viên không được trùng mã hris");
        }
        if(employeeRepository.existsByRmCode(employeeReqDTO.getRmCode())){
            throw new EmployeeException("Nhân viên không được trùng rm_code");
        }
         Optional.of(employeeReqDTO)
                .map(employeeReq ->modelMapper.map(employeeReq,Employee.class))
                .map(employeeRepository::save)
                .map(employee -> modelMapper.map(employee,EmployeeDTO.class))
                .orElseThrow();
    }

    @Override
    public EmployeeDTO update(EmployeeReqDTO employeeReqDTO, Integer id) {
        id = employeeReqDTO.getId();
        return Optional.of(employeeReqDTO)
                .map(employeeReq -> modelMapper.map(employeeReq,Employee.class))
                .map(employeeRepository::save)
                .map(employee -> modelMapper.map(employee,EmployeeDTO.class))
                .orElseThrow();
    }

    @Override
    public void delete(Integer id) {

        employeeRepository.deleteById(id);
    }

    @Override
    public Page<EmployeeDTO> search(String name, Integer code, Pageable pageable) {
        Specification<Employee> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(StringUtils.isNotBlank(name)){
                predicates.add(criteriaBuilder.equal(root.get("userName"),name));
            }
            return query.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
        return employeeRepository.findAll(specification,pageable).map(employee -> modelMapper.map(employee,EmployeeDTO.class));
    }

}
