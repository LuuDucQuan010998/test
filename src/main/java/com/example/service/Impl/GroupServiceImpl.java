package com.example.service.Impl;

import com.example.DTO.GroupDTO;
import com.example.DTO.Req.GroupEmployeeCreateDTO;
import com.example.DTO.Req.GroupEmployeeReqDTO;
import com.example.DTO.Req.GroupReqDTO;
import com.example.entity.Employee;
import com.example.entity.Group;
import com.example.entity.GroupEmployee;
import com.example.exceptions.EmployeeException;
import com.example.exceptions.ExceptionUtils;
import com.example.repository.EmployeeRepository;
import com.example.repository.GroupEmployeeRepository;
import com.example.repository.GroupRepository;
import com.example.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Component
public class GroupServiceImpl implements GroupService {

    private final ModelMapper modelMapper;

    private final GroupRepository groupRepository;

    private final EmployeeRepository employeeRepository;

    private final GroupEmployeeRepository groupEmployeeRepository;


    @Override
    public void create(GroupReqDTO groupReqDTO) throws EmployeeException {

        groupReqDTO.setIsActive(true);
        var g = new Group(groupReqDTO);
        List<GroupEmployee> groupEmployeeList = new ArrayList<>();
        String name = null;
        String code = null;
        if(groupReqDTO.getGroupEmployeeList() == null){
            throw new EmployeeException(
                    ExceptionUtils.messages.get(ExceptionUtils.GROUP_NOT_EXIST)
            );
        }
        var list = groupReqDTO.getGroupEmployeeList().stream().map(group -> group.getIsLeader()).collect(Collectors.toList());
        if(!list.contains(true)){
            throw new EmployeeException(ExceptionUtils.messages.get(ExceptionUtils.GROUP_NOT_LEADER_EXIST));
        }
        var check = true;
        for (GroupEmployeeCreateDTO groupEmployee : groupReqDTO.getGroupEmployeeList()){
            Optional<Employee> employee = employeeRepository.findById(groupEmployee.getEmployeeId());
            if(employee.isEmpty()){
                throw new EmployeeException("nhân viên ko tồn tại");
            }
            Employee em = employee.get();
            if(groupEmployee.getIsLeader()){
                if(check) {
                    check = false;
                    if (em.getIsManager() == false) {
                        throw new EmployeeException("Trưởng nhóm chưa được phân nhóm quản lý");
                    }else {
                        name = em.getDivisionCode() + " " + em.getBranchCode() + " " + em.getFullName();
                        code = em.getDivisionCode() + " " + em.getBranchCode() + " " + em.getHrisCode();
                    }
                }else{
                    throw new EmployeeException("Nhóm chỉ có 1 trưởng nhóm");
                }
            }
            var endDate = em.getGroupEmployeeList().stream().map(ge -> ge.getEndDate()).collect(Collectors.toList());
            if(endDate.contains(null) ){
                throw new EmployeeException("nhan viên đã tồn tại trong nhóm khác");
            }
            GroupEmployee groupE = new GroupEmployee(groupEmployee);
            groupE.setGroup(g);
            groupE.setEmployee(em);
            groupE.setAssignedDate(LocalDate.now());
            groupEmployeeList.add(groupE);
        }

        g.setGroupName(name);
        g.setGroupCode(code);
        if(groupEmployeeList.size() <= 3) {
            groupEmployeeRepository.saveAll(groupEmployeeList);
        }else {
            throw new EmployeeException("Số lượng RM trong nhóm không được vượt quá số lượng quy định ");
        }
        groupRepository.save(g);
    }


    @Override
    public void update(Integer id, GroupEmployeeReqDTO groupEmployeeReqDTOList) throws EmployeeException {
        Optional<Group> group = groupRepository.findById(id);
        String name = null;
        String code = null;
        if(group.isEmpty()){
            throw new EmployeeException("nhóm không tồn tại");
        }
        Group g = group.get();
        var check = true;

        List<GroupEmployee> groupEmployeeList = new ArrayList<>();
        var groupAdd = groupEmployeeReqDTOList.getGroupEmployeeList();
        var groupDelete =groupEmployeeReqDTOList.getGroupEmployeeDeleteList();

        var lead = g.getGroupEmployeeList().stream()
                .filter(gE ->{
                    if(gE.getIsLeader()==true )
                        return true;
                    return false;
                } ).findAny().orElse(null);

        var list = g.getGroupEmployeeList().stream()
                .map(l -> l.getEmployee().getId()).collect(Collectors.toList());
        if(groupAdd != null) {
            var ck = groupAdd.stream().map(gr -> gr.getIsLeader()).collect(Collectors.toList());
            if(!ck.contains(true)){
                throw new EmployeeException(ExceptionUtils.messages.get(ExceptionUtils.GROUP_NOT_LEADER_EXIST));
            }
            for (GroupEmployeeCreateDTO groupEmployee : groupAdd) {
                Optional<Employee> employee = employeeRepository.findById(groupEmployee.getEmployeeId());
                if (employee.isEmpty()) {
                    throw new EmployeeException("nhân viên ko tồn tại");
                }
                Employee em = employee.get();
                var  test = groupEmployeeRepository.findByEmployeeAndAndEndDate(em,null);
                if (groupEmployee.getIsLeader()) {
                    if (check) {
                        if (em.getIsManager() == false){
                            throw new EmployeeException("Trưởng nhóm chưa được phân nhóm quản lý");
                        }else {
                            name = em.getDivisionCode() + " " + em.getBranchCode() + " " + em.getFullName();
                            code = em.getDivisionCode() + " " + em.getBranchCode() + " " + em.getHrisCode();
                            if(lead != null)
                                lead.setEndDate(LocalDate.now());
                        }
                        if(list.contains(groupEmployee.getEmployeeId()) ){
                             test.setEndDate(LocalDate.now());
                        }

                    }
                }
                if(groupEmployee.getEmployeeId() == em.getId()
                        && groupEmployee.getIsLeader() == test.getIsLeader())
                        {
                          if(test.getGroup().getId() != id) {
                              throw new EmployeeException("Nhân viên đã tồn tại trong nhóm khác");
                          }
                          else
                              throw new EmployeeException("Nhân viên đã có trong nhóm");

                }

                GroupEmployee groupE = new GroupEmployee(groupEmployee);
                groupE.setGroup(g);
                groupE.setEmployee(em);
                groupE.setAssignedDate(LocalDate.now());
                groupEmployeeList.add(groupE);
            }
            g.setGroupName(name);
            g.setGroupCode(code);
            if(groupEmployeeList.size() <= 50) {
                groupEmployeeRepository.saveAll(groupEmployeeList);
            }else {
                throw new EmployeeException("Số lượng RM trong nhóm không được vượt quá số lượng quy định ");
            }
            groupRepository.save(g);
        }
        List<GroupEmployee> groupDeleteList = new ArrayList<>();
        if(groupDelete != null)
            for (GroupEmployeeCreateDTO groupEmployee : groupDelete) {
                Optional<Employee> employee = employeeRepository.findById(groupEmployee.getEmployeeId());
                if (employee.isEmpty()) {
                    throw new EmployeeException("Nhân viên không tồn tại");
                }
                var e = employee.get();
                GroupEmployee ge = groupEmployeeRepository.findByEmployee(e);
                if (ge.getEndDate() != null) {
                    throw new EmployeeException("Nhân viên đã rời nhóm");
                } else
                    ge.setEndDate(LocalDate.now());
                groupEmployeeRepository.save(ge);
            }
    }

    //Xóa group
    @Override
    public void delete(Integer id) throws EmployeeException {
        Optional<Group> group = groupRepository.findById(id);
        if(group.isEmpty()){
            throw new EmployeeException("Nhóm ko tồn tại");
        }
        var g = group.get();
        g.setIsActive(false);
        List<GroupEmployee> groupEmplyeeList = new ArrayList<>();
        for (GroupEmployee gE : g.getGroupEmployeeList()){
            if(gE.getEndDate()==null){
                gE.setEndDate(LocalDate.now());
            }
            groupEmplyeeList.add(gE);
        }
        groupEmployeeRepository.saveAll(groupEmplyeeList);
    }
}
