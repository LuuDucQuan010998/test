package com.example.exceptions;

import java.util.HashMap;
import java.util.Map;

public class ExceptionUtils {
    public static final String E_INTERNAL_SERVER = "E_INTERNAL_SERVER";
    public static final String GROUP_NOT_EXIST = "GROUP_NOT_EXIST";
    public static final String GROUP_NOT_LEADER_EXIST = "GROUP_NOT_LEADER_EXIST";
    public static Map<String, String> messages;

    static {
        messages = new HashMap<>();
        messages.put(ExceptionUtils.E_INTERNAL_SERVER, "Server không phản hồi");
        messages.put(ExceptionUtils.GROUP_NOT_EXIST, "Nhóm không tồn tại");
        messages.put(ExceptionUtils.GROUP_NOT_LEADER_EXIST, "Nhóm phải tồn tại 1 trưởng nhóm");
    }
    public static String buildMessage(String messKey, Object... arg) {
        return String.format(ExceptionUtils.messages.get(messKey), arg);
    }
}
