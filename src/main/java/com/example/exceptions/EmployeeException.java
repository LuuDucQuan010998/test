package com.example.exceptions;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class EmployeeException extends Exception {

  private String messageKey;
  private String message;
  private Throwable throwable;
  private List<String> messages;

  public EmployeeException(String msgKey) {
    this.messageKey = msgKey;
  }

  public EmployeeException(String msgKey, String msg) {
    this.messageKey = msgKey;
    this.message = msg;
  }

  public String getMessage() {
    if (this.message != null) {
      return message;
    }
    if (this.messageKey != null) {
      this.message = ExceptionUtils.messages.get(this.messageKey);
    }
    return null;
  }
}
