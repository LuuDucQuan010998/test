package com.example.repository;

import com.example.entity.Employee;
import com.example.entity.Group;
import com.example.entity.GroupEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface GroupEmployeeRepository extends JpaRepository<GroupEmployee,Integer> {
    GroupEmployee findByEmployee(Employee employee);
    GroupEmployee findByEmployeeAndAndEndDate(Employee employee,LocalDate date);
    List<GroupEmployee> findByEndDateAndGroup(LocalDate end , Group group);
    GroupEmployee findByIsLeader(Boolean lead);
}
