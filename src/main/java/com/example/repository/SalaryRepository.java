package com.example.repository;

import com.example.entity.Salary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Integer> , JpaSpecificationExecutor<Salary> {

    @Query("SELECT s FROM Salary s")
    Page<Salary> findSalarys(Pageable pageable);

    @Query(value = "select s from Salary s where s.employeeName = :name  or s.employeeCode = :code")
    List<Salary> findByName(@Param("name") String name, @Param("code") Integer code );

}
