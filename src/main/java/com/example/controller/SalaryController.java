package com.example.controller;

import com.example.DTO.Req.SalaryReqDTO;
import com.example.DTO.SalaryDTO;
import com.example.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class SalaryController {


    private final SalaryService salarySevice;

    @GetMapping("/get-all-salary")
    public List<SalaryDTO> getAllSalary() {
        return salarySevice.getAll();
    }

    @GetMapping("/get-salary")
    public Page<SalaryDTO> getPaseSalary() {
        return salarySevice.getAllandPase();
    }

    @GetMapping("/sum-salary")
    public Double SumSalary() {
        return salarySevice.sumSalary();
    }

    @GetMapping("/search")
    public Page<SalaryDTO> search(@RequestParam(required = false) Integer code, @RequestParam(required = false) String name,
                                  @RequestParam Integer page , @RequestParam Integer size) {
        Pageable pageable = PageRequest.of(page,size);
        return salarySevice.search(code, name , pageable);

    }

    @GetMapping("/search2")
    // sao lại bắt người dùng truyền ntn =))
    public Page<SalaryDTO> search2(@RequestParam(required = false) Integer code, @RequestParam(required = false) String name,
                                  @RequestParam Integer page , @RequestParam Integer size) {
        Pageable pageable = PageRequest.of(page,size);
        return salarySevice.search2(code, name , pageable);

    }

    @GetMapping("/get-name")
    public List<SalaryDTO> getSalaryName(@RequestParam String name, @RequestParam Integer code) {
        return salarySevice.getName(name, code);
    }


    @PostMapping("/salary" )
    public ResponseEntity<SalaryDTO> addSalary(@RequestBody SalaryReqDTO salary) {
        return ResponseEntity.ok(salarySevice.add(salary));
    }

    @PutMapping("/salary/{id}")
    public ResponseEntity<SalaryDTO> updateSalary(@PathVariable("id") Integer id, @RequestBody SalaryReqDTO salary) {
        return ResponseEntity.ok(salarySevice.update(id,salary));
    }

    @DeleteMapping("/salary/{id}")
    public void deleteStudent(@PathVariable("id") Integer id) {
        salarySevice.delete(id);
    }

}
