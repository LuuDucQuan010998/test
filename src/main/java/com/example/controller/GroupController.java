package com.example.controller;

import com.example.DTO.ErrorDTO;
import com.example.DTO.Req.GroupEmployeeReqDTO;
import com.example.DTO.Req.GroupReqDTO;
import com.example.exceptions.EmployeeException;
import com.example.exceptions.ExceptionUtils;
import com.example.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;

    @PostMapping("/group")
    public ResponseEntity<Object> create(@RequestBody GroupReqDTO group)   {
        try{
            groupService.create(group);
        }catch (EmployeeException e) {
                return new ResponseEntity<>(
                    new ErrorDTO(e.getMessage(),e.getMessageKey()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
                    HttpStatus
                            .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> update(@RequestBody GroupEmployeeReqDTO groupEmployeeReqDTOList, @PathVariable Integer id)  {
        try{
            groupService.update(id, groupEmployeeReqDTOList);
        }catch (EmployeeException e) {
            return new ResponseEntity<>(
                    new ErrorDTO(e.getMessageKey(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e) {
            return new ResponseEntity<>(
                    ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
                    HttpStatus
                            .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @PutMapping("/group/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Integer id) {

        try{
            groupService.delete(id);
        }catch (EmployeeException e) {
            return new ResponseEntity<>(
                    new ErrorDTO(e.getMessageKey(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e) {
            return new ResponseEntity<>(
                    ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
                    HttpStatus
                            .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
