package com.example.controller;

import com.example.DTO.EmployeeDTO;
import com.example.DTO.ErrorDTO;
import com.example.DTO.Req.EmployeeReqDTO;
import com.example.exceptions.EmployeeException;
import com.example.exceptions.ExceptionUtils;
import com.example.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api1")
public class EmployeeController {

    private final EmployeeService employeeService;


    @PostMapping("/employee")
    public ResponseEntity<Object> create(@RequestBody EmployeeReqDTO employee){

        try{
            employeeService.create(employee);
        }catch (EmployeeException e) {
            return new ResponseEntity<>(
                    new ErrorDTO(e.getMessage(),e.getMessageKey()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
                    HttpStatus
                            .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<EmployeeDTO> update(@PathVariable("id") Integer id, @RequestBody EmployeeReqDTO employee){
        return ResponseEntity.ok(employeeService.update(employee,id));
    }

    @DeleteMapping("/employee/{id}")
    public void delete(@PathVariable("id") Integer id){
        employeeService.delete(id);
    }

    @GetMapping("/search")
    public Page<EmployeeDTO> search(@RequestParam(required = false) String name, @RequestParam(required = false) Integer code,
                                  @RequestParam Integer page, @RequestParam Integer size){
        Pageable pageable = PageRequest.of(page,size);
        return employeeService.search(name,code,pageable);
    }

}
