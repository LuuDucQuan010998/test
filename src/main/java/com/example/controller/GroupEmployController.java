package com.example.controller;

import com.example.DTO.GroupEmployeeDTO;
import com.example.DTO.Req.GroupEmployeeReqDTO;
import com.example.service.GroupEmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api1")
@RequiredArgsConstructor
public class GroupEmployController {

    private final GroupEmployeeService groupEmployeeService;

    @PostMapping("/groupEmployee")
    public ResponseEntity<GroupEmployeeDTO> create(@RequestBody GroupEmployeeDTO group){
        return ResponseEntity.ok(groupEmployeeService.create(group));
    }

    @PutMapping("/groupEmployee/{id}")
    public ResponseEntity<GroupEmployeeDTO> update(@PathVariable("id") Integer id, @RequestBody GroupEmployeeDTO group){
        return ResponseEntity.ok(groupEmployeeService.update(id,group));
    }

    @DeleteMapping("/groupEmployee/{id}")
    public void delete(@PathVariable("id") Integer id){
        groupEmployeeService.delete(id);
    }
}
