package com.example.DTO;

import com.example.entity.Employee;
import com.example.entity.Group;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupEmployeeDTO {

    private Integer id;
    private Group group;
    private Employee employee;
    private LocalDate assignedDate;
    private Boolean isLeader;
    private LocalDate endDate;


}
