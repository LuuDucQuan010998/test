package com.example.DTO;

import com.example.entity.Employee;
import com.example.entity.GroupEmployee;
import com.example.entity.Salary;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {
    private Integer id;
    private String userName ;
    private String fullName;
    private String rmCode;
    private String hrisCode;
    private String branchCode;
    private String divisionCode;
    private String phone;
    private String email;
    private String position;
    private Boolean isManager;
    private LocalDate dateOfBirth;


}
