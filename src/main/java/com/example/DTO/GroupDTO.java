package com.example.DTO;

import com.example.entity.GroupEmployee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupDTO {
    private Integer id;
    private String groupCode;
    private String groupName;
    private Integer divisionCode;
    private Boolean isActive;
    private List<GroupEmployee> groupEmployeeList;

}
