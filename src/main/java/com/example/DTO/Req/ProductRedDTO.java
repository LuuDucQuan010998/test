package com.example.DTO.Req;

import com.example.entity.Category;
import lombok.Data;

import java.util.List;

@Data
public class ProductRedDTO {
    private Integer id;
    private String name;
    private List<Category> categoryList;
}
