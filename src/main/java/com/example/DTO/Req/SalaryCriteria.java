package com.example.DTO.Req;

import lombok.Data;

@Data
public class SalaryCriteria {
    private Integer code;
    private String name;
}
