package com.example.DTO.Req;

import lombok.Data;

import java.time.LocalDate;

@Data
public class EmployeeCreateDTO {

    private String fullName;
    private Integer rmCode;
    private String position;
    private boolean isManager;

}
