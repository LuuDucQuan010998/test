package com.example.DTO.Req;

import com.example.entity.Employee;
import lombok.Data;

import java.util.List;



@Data
public class SalaryReqDTO {
    private int id;
    private int employeeCode;
    private String employeeName;
    private double salary;
    private int month;
    private int year;
    private String company;
    private List<Employee> employees;

}
