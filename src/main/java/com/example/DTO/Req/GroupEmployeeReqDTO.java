package com.example.DTO.Req;

import com.example.entity.Employee;
import com.example.entity.Group;
import com.example.entity.GroupEmployee;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class GroupEmployeeReqDTO {

   private List<GroupEmployeeCreateDTO> groupEmployeeList;

   private List<GroupEmployeeCreateDTO> groupEmployeeDeleteList;

}
