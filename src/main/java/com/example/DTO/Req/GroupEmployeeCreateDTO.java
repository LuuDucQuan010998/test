package com.example.DTO.Req;

import lombok.Data;

import java.time.LocalDate;

@Data
public class GroupEmployeeCreateDTO {
    private Integer employeeId;
    private Boolean isLeader;
    private LocalDate assignedDate;
    private LocalDate endDate;
}
