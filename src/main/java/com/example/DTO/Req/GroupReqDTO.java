package com.example.DTO.Req;

import com.example.entity.GroupEmployee;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GroupReqDTO {

    private Integer divisionCode;
    private Boolean isActive;
    private List<GroupEmployeeCreateDTO> groupEmployeeList;
}
