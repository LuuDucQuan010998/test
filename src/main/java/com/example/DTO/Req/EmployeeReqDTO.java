package com.example.DTO.Req;

import com.example.entity.GroupEmployee;
import com.example.entity.Salary;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class EmployeeReqDTO {
    private Integer id;
    private String userName ;
    private String fullName;
    private String rmCode;
    private String hrisCode;
    private String branchCode;
    private String divisionCode;
    private String phone;
    private String email;
    private String position;
    private Boolean isManager;
    private LocalDate dateOfBirth;
    private List<GroupEmployee> groupEmployeeList;

}
