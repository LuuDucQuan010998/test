package com.example.DTO.Req;

import com.example.entity.Product;
import lombok.Data;

import java.util.List;

@Data
public class CategoryRedDTO {
    private Integer id;
    private String name;
    private List<Product> productList;
}
