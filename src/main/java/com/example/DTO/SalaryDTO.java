package com.example.DTO;

import com.example.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalaryDTO {
    private int id;
    private int employeeCode;
    private String employeeName;
    private double salary;
    private int month;
    private int year;
    private String company;
    private List<Employee> employees;
}
